def _initialise_rings(prec=53):
    global PREC
    global RRING
    global CRING
    global TWO_PI
    global TWO_PI_I
    global CZERO
    PREC = prec
    RRING = RealIntervalField(PREC)
    CRING = ComplexIntervalField(PREC)
    TWO_PI = RRING(2*pi)
    TWO_PI_I = TWO_PI*CRING(0, 1)
    CZERO = CRING(0, 0)
    return CRING


def eigenvalue_numerical(f, p, err, verbose=False, prec=None):
    k = f.weight()
    if prec is None:
        prec = _estimate_prec(k, p, err)
        prec = prec + 3    # for luck, but mostly to avoid the issues with level 2 and 3
        estimated = True
    else:
        estimated = False
    CRING = _initialise_rings(prec)
    z = CRING(0, 1.1)
    _clear_cache(f)

    eps = sign(atkin_lehner_eigenvalue(f, z))
    ev, _, _ = _eigenvalue_Tp_with_error(f, z, p, err, eps, verbose=verbose)

    if _diameter(ev) > err:
        if estimated:
            st = 'auto-estimated'
        else:
            st = 'given'
        raise RuntimeError("%s precision %s was insufficient to compute the eigenvalue %s to requested accuracy; please increase the precision via the optional argument 'prec='" % (st, prec, ev))
    return ev


def _eigenvalue_Tp_with_error(F, z, p, err, eps, verbose=False):
    k = F.weight()

    f, tilde_eps_f = _rough_estimate_F(F, z, eps)
    fl = abs(f) - 2*tilde_eps_f
    fu = abs(f) + 2*tilde_eps_f

    deligne = RDF(p)^((k-1)/2)
    tpfu = deligne*fu
    tilde_eps_tpf = deligne*tilde_eps_f

    eps_tpf = min([err*fl/2, tilde_eps_tpf])
    eps_f = min([err*fl^2/(2*tpfu), tilde_eps_f])

    f, fT = _eval_F_with_error(F, z, eps_f, eps)

    tpf, tpfT = _eval_TpF_with_error(F, z, p, eps_tpf, eps)
    ev = tpf/f
    return ev, fT, tpfT


def _rough_estimate_F(f, z, eps, mag=10):
    tilde_eps_f = 1/mag
    val, _ = _eval_F_with_error(f, z, tilde_eps_f, eps)
    while (abs(val) - 2*tilde_eps_f) < 0:
        tilde_eps_f = tilde_eps_f/mag
        val, _ = _eval_F_with_error(f, z, tilde_eps_f, eps)
    return val, tilde_eps_f


def _eval_TpF_with_error(f, z, p, err, eps, verbose=False):
    s = CZERO
    k = f.weight()
    err_f = err/(p+1)
    # vTs = []
    max_T = 0
    for (t, newZ) in _make_data(z, k, p):
        if (t == p):
            scalar = p**(k-1)
        else:
            scalar = 1/p
        v, T = _eval_F_with_error(f, newZ, err_f/scalar, eps)
        # vTs.append((v, T))
        if T > max_T:
            max_T = T
        if (t == 0) or (t == p) or (p == 2):
            s = s + scalar*v
        else:
            s = s + scalar*2*v.real()
    return s, max_T


def _eval_F_with_error(f, z, err, eps, optimise_imaginary_part=True, verbose=False):
    mult = 1
    if optimise_imaginary_part:
        z, mult = _optimize_imaginary_part(z, f.weight(), f.level(), eps)
    T = _find_T(f, z, err/abs(mult))
    res = mult * _eval_F_up_to(f, z, T)
    return res, T


def _find_T(f, z, err):
    """
    Note that this uses Deligne's bound, so it's only guaranteed to
    give correct results when applied to a cuspidal eigenform.
    """
    # TODO: can we improve the bound?
    # TODO: introduce optional parameter is_cuspidal, and when False use appropriate bound
    k = f.weight()
    if (k % 2) == 1:
        d = (k+1)/2
    else:
        d = (k+2)/2
    alpha = TWO_PI*z.imag()
    bound = err*alpha/(d+1)
    e_alpha = (-alpha).exp()
    start = RRING(d/alpha).unique_floor() + 1
    for T in xrange(start, 100000):
        test = e_alpha^T * T^d
        if test < bound:
            return ZZ(T)
    return 0


def _eval_F_up_to(f, z, Tmax):
    q = (TWO_PI_I*z).exp()
    flst = _get_coeff_list(f, Tmax)
    s = CZERO
    for T in xrange(Tmax, -1, -1):
        s = flst[T] + q*s
    return s


def _clear_cache(f):
    try:
        del(f.__emb)
        del(f.__q_expansion_list)
    except AttributeError:
        pass


def _get_coeff_list(f, Tmax):
    # see if we already have a cached embedding for f
    try:
        emb = f.__emb
    except AttributeError:
        emb = f.base_ring().embeddings(CRING)[0]
        f.__emb = emb
    # see if we already have cached coefficient list for f
    try:
        current_Tmax, flst = f.__q_expansion_list
    except AttributeError:
        current_Tmax = 0
        flst = [emb(f[0])]
    # if the cached list does not extend to Tmax, recalculate it to Tmax and cache it
    if current_Tmax < Tmax:
        # TODO: could maybe make this a little quicker by extending the cached list, rather than recalculating it
        flst = f.q_expansion(Tmax+1).list()
        flst = [emb(c) for c in flst]
        f.__q_expansion_list = (Tmax, flst)
    return flst


def _optimize_imaginary_part(z, k, N, eps):
    """
    Given z in the upper half plane and a weight k,
    return the unique representative z0 of z in the
    fundamental domain for SL(2,Z), as well as the
    complex number mult such that

        f(z) = mult * f(z0)
    """
    mult = 1
    x = z.real()
    y = z.imag()
    m = _round(x)
    x = x - m
    (a, b, c, d) = (1, -m, 0, 1)
    dist = x^2 + y^2
    sign = CRING(1, 0)
    while dist < 1/N:
        x = -x/(N*dist)
        y = y/(N*dist)
        m = _round(x)
        x = x - m
        (a, b, c, d) = (-a*m*N-c, -b*m*N-d, a*N, b*N)
        dist = x^2 + y^2
        sign = sign*eps
    res = CRING(x, y)
    det = a*d-b*c
    mult = sign*det^(k/2)*(c*z+d)^(-k)
    return res, mult


def _diameter(x):
    left, right = x.real().endpoints()
    return right - left


def _round(x):
    return x.endpoints()[0].round()


def _slash_operator(m, z):
    (a, b, c, d) = m
    # return (a*z+b)/(c*z+d)
    x = z.real()
    y = z.imag()
    den = (c*x+d)^2 + c^2*y^2
    newX = (a*x+b)*(c*x+d) + a*c*y^2
    newY = y*(a*d-b*c)
    return CRING(newX/den, newY/den)


def _make_data(z, k, p):
    tp_mats = _tp_cosets(p)
    lst = []
    for tt in tp_mats:
        newZ = _slash_operator(tt, z)
        if (tt[0] == p):
            lst.append((p, newZ))
        else:
            lst.append((tt[1], newZ))
    return lst


def _tp_cosets(p):
    if (p == 2):
        return _t2_cosets()
    else:
        return _tnot2_cosets(p)


def _tnot2_cosets(p):
    lst = [(p, 0, 0, 1)]
    for b in range((p+1)/2):
        lst.append((1, b, 0, p))
    return lst


def _t2_cosets():
    return [(2, 0, 0, 1),
            (1, 0, 0, 2),
            (1, 1, 0, 2)]


def _min_prec(f, p, err, start_prec=2):
    for prec in range(start_prec, 2000):
        _initialise_rings(prec)
        _clear_cache(f)
        try:
            z = CRING(0, 1.1)
            ev, _, _ = _eigenvalue_Tp_with_error(f, z, p, err)
            if _diameter(ev) < err:
                return prec
        except ValueError:
            pass
    raise RuntimeError("required precision is greater than 2000")


def _estimate_prec(k, p, err):
    """
    This was only tested with z = 1.1*i.
    """
    logp = RR(ln(p))
    m = -RR(log(err, 10))
    s = RR(10/3*m)
    # s = s + k*(0.709957*logp+0.100201)
    # s = s + 1.855215*logp + 4.979
    s = s + k*(0.72*logp + 0.11)
    s = s + 2*logp + 5
    return s.ceil()


def atkin_lehner_eigenvalue(f, z):
    N = f.level()
    if N == 1:
        return 1
    if N in [2, 3]:
        return -sign(f[N])
    raise ValueError("Atkin-Lehner eigenvalue only implemented for levels 1, 2, or 3.")


def atkin_lehner_eigenvalue_numerical(f, z):
    # incorrect answer for N=2, k=10
    epsf = _sign_numerical(f, z)
    k = f.weight()
    p = f.level()
    epsalf = (-1)^(k/2)*_sign_numerical(f, (-1)/(p*z))
    if epsf == epsalf:
        return 1
    else:
        return -1


def _sign_numerical(f, z):
    for j in range(1, z.prec()):
        err = 2**(-j)
        res = _eval_F_with_error(f, z, err, 1)[0].real()
        if abs(res) > err:
            return sgn(res)
    raise RuntimeError("The precision of z is not sufficient to find the sign of f(z)")
