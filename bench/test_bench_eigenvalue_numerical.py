import pytest
from sage.all_cmdline import next_prime, Newforms
from src.eigenvalue_sage import eigenvalue_numerical

k = 12
p = next_prime(1000)
f = Newforms(1, k, names='a')[0]

def test_bench_coeff(benchmark):
    res = benchmark(_coeff, f, p)
    assert True


def test_bench_num(benchmark):
    err = 0.1
    res = benchmark(eigenvalue_numerical, f, p, err)
    assert True


def _coeff(f, p):
    ms = f.modular_symbols(1)
    T = ms._eigen_nonzero_element(p)
    ev = ms._element_eigenvalue(T, name='a')
    return ev
