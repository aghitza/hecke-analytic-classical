import pytest
from sage.all_cmdline import Newforms, prime_range, CuspForms, sign
from src.eigenvalue_sage import atkin_lehner_eigenvalue, _initialise_rings, _sign_numerical


class TestAtkinLehner(object):

    def test_weight8_level2(self):
        CRING = _initialise_rings()
        z = CRING(0, 1.1)
        f = Newforms(2, 8, names='a')[0]
        eps = atkin_lehner_eigenvalue(f, z)
        assert eps == +1

    def test_level2_loop(self):
        CRING = _initialise_rings()
        z = CRING(0, 1.1)
        for k in range(10, 31, 2):
            for f in Newforms(2, k, names='a'):
                eps = atkin_lehner_eigenvalue(f, z)
                assert eps == sign(f.atkin_lehner_eigenvalue())

    def test_weight6_level3(self):
        CRING = _initialise_rings()
        z = CRING(0, 1.1)
        f = Newforms(3, 6, names='a')[0]
        eps = atkin_lehner_eigenvalue(f, z)
        assert eps == -1

    def test_level3_loop(self):
        CRING = _initialise_rings()
        z = CRING(0, 1.1)
        for k in range(8, 25, 2):
            for f in Newforms(3, k, names='a'):
                eps = atkin_lehner_eigenvalue(f, z)
                assert eps == sign(f.atkin_lehner_eigenvalue())


class TestSign(object):

    def test_delta_at_i(self):
        f = CuspForms(1, 12).gen(0)
        CRING = _initialise_rings()
        z = CRING(0, 1)
        eps = _sign_numerical(f, z)
        assert eps == 1

    def test_minus_delta_at_i(self):
        f = CuspForms(1, 12).gen(0)
        CRING = _initialise_rings()
        z = CRING(0, 1)
        eps = _sign_numerical(-f, z)
        assert eps == -1
