import pytest
from sage.all_cmdline import sqrt
from src.eigenvalue_sage import _optimize_imaginary_part, _round, _initialise_rings


class TestOptimizeImaginary(object):

    # Level one

    def test_opt_imag_trivial(self):
        CRING = _initialise_rings()
        z = CRING(0, 2)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, z) and equal(mult, 1)

    def test_opt_imag_boundary_one(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, z) and equal(mult, 1)

    def test_opt_imag_boundary_two(self):
        CRING = _initialise_rings()
        z = CRING(0.4999, 2)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, z) and equal(mult, 1)

    def test_opt_imag_boundary_three(self):
        CRING = _initialise_rings()
        z = CRING(-0.4999, 1)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, z) and equal(mult, 1)

    def test_opt_imag_boundary_four(self):
        CRING = _initialise_rings()
        z = CRING(0, 1.0001)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, z) and equal(mult, 1)

    def test_opt_imag_boundary_five(self):
        CRING = _initialise_rings()
        z = CRING(-0.49999, sqrt(3)/2)
        w = CRING(0.49999, sqrt(3)/2)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, w) and equal(mult, 1)

    def test_opt_imag_one_inversion(self):
        CRING = _initialise_rings()
        z = CRING(0.5, 0.5)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, CRING(0, 1)) and equal(mult, z**(-12))

    def test_opt_imag_translate_left(self):
        CRING = _initialise_rings()
        z = CRING(3.1, 2)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, CRING(0.1, 2)) and equal(mult, 1)

    def test_opt_imag_translate_right(self):
        CRING = _initialise_rings()
        z = CRING(-3.1, 2)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, CRING(-0.1, 2)) and equal(mult, 1)

    def test_opt_imag_mixed(self):
        CRING = _initialise_rings()
        z = CRING(0.4, 0.1)
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, CRING(-0.25, 1.25)) and equal(mult, z**(-12)*(CRING(-6, 10)/17.0)**(-12))

    def test_opt_imag_one(self):
        CRING = _initialise_rings()
        z = CRING(3**(-1), 3**(-1))
        res, mult = _optimize_imaginary_part(z, 12, 1, 1)
        assert equal(res, CRING(0.5, 1.5))

    # Level two

    def test_opt_imag_on_yaxis(self):
        CRING = _initialise_rings()
        z = CRING(0, 3**(-1))
        res, mult = _optimize_imaginary_part(z, 12, 2, -1)
        assert equal(res, CRING(0, 1.5)) and equal(mult, -z**(-12)*2**(-6))

    def test_opt_imag_off_yaxis(self):
        CRING = _initialise_rings()
        z = CRING(3**(-1), 3**(-1))
        res, mult = _optimize_imaginary_part(z, 12, 2, -1)
        assert equal(res, CRING(0.25, 0.75)) and equal(mult, -z**(-12)*2**(-6))

    def test_opt_imag_off_yaxis(self):
        CRING = _initialise_rings()
        z = CRING(2*3**(-1), 3**(-1))
        res, mult = _optimize_imaginary_part(z, 12, 2, -1)
        assert equal(res, CRING(-0.25, 0.75)) and equal(mult, -(z-1)**(-12)*2**(-6))

    def test_opt_imag_off_yaxis_two_inversions(self):
        CRING = _initialise_rings()
        z = CRING(5*11**(-1), 11**(-1))
        res, mult = _optimize_imaginary_part(z, 12, 2, -1)
        mult_guess = 2**(-12)*z**(-12)*CRING(-3*52**(-1), 11*52**(-1))**(-12)
        assert equal(res, CRING(-0.4, 2.2)) and equal(mult, mult_guess)

    # Level three

    def test_opt_imag_on_yaxis(self):
        CRING = _initialise_rings()
        z = CRING(0, 5**(-1))
        res, mult = _optimize_imaginary_part(z, 12, 3, -1)
        assert equal(res, CRING(0, 5*3**(-1))) and equal(mult, -z**(-12)*3**(-6))

    def test_opt_imag_off_yaxis(self):
        CRING = _initialise_rings()
        z = CRING(3*5**(-1), 5**(-1))
        res, mult = _optimize_imaginary_part(z, 12, 3, -1)
        mult_guess = (z-1)**(-12)*CRING(-3**(-1), 3**(-1))**(-12)*3**(-12)
        assert equal(res, CRING(0.5, 0.5)) and equal(mult, mult_guess)

    def test_opt_imag_off_yaxis_two_inversions(self):
        CRING = _initialise_rings()
        z = CRING(5*11**(-1), 11**(-1))
        res, mult = _optimize_imaginary_part(z, 12, 3, -1)
        mult_guess = -3**(-18)*z**(-12)*CRING(23*78**(-1), 11*78**(-1))**(-12)*CRING(2*25**(-1), 11*25**(-1))**(-12)
        assert equal(res, CRING(-2*15**(-1), 11*15**(-1))) and equal(mult, mult_guess)


class TestRound(object):

    def test_round_one(self):
        CRING = _initialise_rings()
        z = CRING(1.234, 0)
        x = z.real()
        m = _round(x)
        assert m == 1

    def test_round_two(self):
        CRING = _initialise_rings()
        z = CRING(1.789, 0)
        x = z.real()
        m = _round(x)
        assert m == 2

    def test_round_three(self):
        CRING = _initialise_rings()
        z = CRING(-1.789, 0)
        x = z.real()
        m = _round(x)
        assert m == -2

    def test_round_four(self):
        CRING = _initialise_rings()
        z = CRING(-1.234, 0)
        x = z.real()
        m = _round(x)
        assert m == -1

    def test_round_four(self):
        CRING = _initialise_rings()
        z = CRING(1.5, 0)
        x = z.real()
        m = _round(x)
        assert m == 2

    def test_round_four(self):
        CRING = _initialise_rings()
        z = CRING(-1.5, 0)
        x = z.real()
        m = _round(x)
        assert m == -2

EPS = 0.0005


def equal(left, right):
    return abs(left-right) < EPS


