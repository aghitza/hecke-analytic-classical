import pytest
from sage.all_cmdline import CuspForms, EisensteinForms, exp, pi
from src.eigenvalue_sage import _initialise_rings, _eval_F_up_to, _eval_F_with_error


class TestEval(object):

    def test_eval_at_i_truncate_at_two(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 12).gen(0)
        res = _eval_F_up_to(f, z, 2)
        assert equal(res, CRING(exp(-2*pi)+f[2]*exp(-4*pi)))

    def test_eval_at_i_truncate_at_ten(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 12).gen(0)
        res = _eval_F_up_to(f, z, 10)
        s = sum([CRING(f[j]*exp(-2*pi*j)) for j in range(1, 11)])
        assert equal(res, s)

    def test_eval_with_error_at_i_E4(self):
        """
        Note: it's a bit lucky that this test passes, since
        the estimate for T is only proved to be correct for
        cusp forms.
        """
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = EisensteinForms(1, 4).gen(0)
        correct = 1.4557628922687093224624220036
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err

    def test_eval_with_error_at_i_Delta12(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 12).gen(0)
        correct = 0.00178536985064215
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err
        err = 10**(-16)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err

    def test_eval_with_error_at_i_Delta16(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 16).gen(0)
        correct = 0.0025990751775401726
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err
        err = 10**(-16)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err

    def test_eval_with_error_at_i_Delta18(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 18).gen(0)
        correct = 0
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err
        err = 10**(-16)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err

    def test_eval_with_error_at_i_Delta20(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 20).gen(0)
        correct = 0.0037836371976796908
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err
        err = 10**(-16)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err

    def test_eval_with_error_at_i_Delta22(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 22).gen(0)
        correct = 0
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err
        err = 10**(-16)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err

    def test_eval_with_error_at_i_Delta26(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 26).gen(0)
        correct = 0
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err
        err = 10**(-16)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - CRING(correct)) < err

    def test_eval_with_error_Delta12(self):
        CRING = _initialise_rings(100)
        z = CRING(3**(-1), 3**(-1))
        f = CuspForms(1, 12).gen(0)
        correct = CRING(0.6714088412656641345338180197, 0)
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - correct) < err
        err = 10**(-15)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - correct) < err

    def test_eval_with_error_Delta16(self):
        CRING = _initialise_rings()
        z = CRING(3**(-1), 3**(-1))
        f = CuspForms(1, 16).gen(0)
        correct = CRING(-13.33289392421168035280177567, 0)
        err = 10**(-2)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - correct) < err
        err = 10**(-9)
        res, T = _eval_F_with_error(f, z, err, 1)
        assert abs(res - correct) < err


EPS = 0.0005


def equal(left, right):
    return abs(left-right) < EPS


