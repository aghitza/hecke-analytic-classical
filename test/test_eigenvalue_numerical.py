import pytest
from sage.all_cmdline import Newforms, prime_range
from src.eigenvalue_sage import eigenvalue_numerical


class TestEigenvalueNumerical(object):

    def test_Delta12_2(self):
        f = Newforms(1, 12, names='a')[0]
        p = 2
        err = 0.1
        res = eigenvalue_numerical(f, p, err)
        assert abs(res - f[p]) < err

    def test_Delta12_loop(self):
        f = Newforms(1, 12, names='a')[0]
        err = 0.1
        for p in prime_range(100):
            res = eigenvalue_numerical(f, p, err)
            assert abs(res - f[p]) < err

    def test_Delta18_loop(self):
        f = Newforms(1, 18, names='a')[0]
        err = 0.1
        for p in prime_range(100):
            res = eigenvalue_numerical(f, p, err)
            assert abs(res - f[p]) < err

    def test_wt24_loop(self):
        f = Newforms(1, 24, names='a')[0]
        for p in prime_range(50):
            for err in [0.1, 0.01, 0.000001]:
                res = eigenvalue_numerical(f, p, err)
                CRING = res.parent()
                assert abs(res - CRING(f[p].complex_embedding(res.prec()))) < err

    def test_insufficient_precision(self):
        f = Newforms(1, 82, names='a')[0]
        p = 29
        err = 0.1
        with pytest.raises(RuntimeError):
            res = eigenvalue_numerical(f, p, err, prec=53)


class TestEigenvalueNumericalLevelTwoOrThree(object):
    
    # the following should not succeed, since 2 divides the level so f[2] is the eigenvalue of U_2, not T_2
    @pytest.mark.xfail
    def test_wt8(self):
        f = Newforms(2, 8, names='a')[0]
        p = 2
        err = 0.1
        res = eigenvalue_numerical(f, p, err)
        assert abs(res - f[p]) < err

    def test_wt8_loop(self):
        f = Newforms(2, 8, names='a')[0]
        for p in prime_range(3, 50):
            for err in [0.1, 0.01, 0.000001]:
                res = eigenvalue_numerical(f, p, err)
                assert abs(res - f[p]) < err

    def test_wt30_loop(self):
        for f in Newforms(2, 8, names='a'):
            for p in prime_range(3, 50):
                for err in [0.1, 0.01, 0.000001]:
                    res = eigenvalue_numerical(f, p, err)
                    assert abs(res - f[p]) < err

    # this goes through all rational newforms of level 2
    def test_level2_loop(self):
        p = 17
        err = 0.1
        for k in range(2, 49, 2):
            for f in Newforms(2, k, names='a'):
                if f.base_ring().degree() == 1:
                    res = eigenvalue_numerical(f, p, err)
                    assert abs(res - f[p]) < err

    # the following should not succeed, since 3 divides the level so f[3] is the eigenvalue of U_3, not T_3
    # but it succeeds for some reason
    def test_wt6(self):
        f = Newforms(3, 6, names='a')[0]
        p = 3
        err = 0.1
        res = eigenvalue_numerical(f, p, err)
        assert abs(res - f[p]) < err

    def test_wt6_loop(self):
        f = Newforms(3, 6, names='a')[0]
        for p in prime_range(5, 50):
            for err in [0.1, 0.01, 0.000001]:
                res = eigenvalue_numerical(f, p, err)
                assert abs(res - f[p]) < err

    def test_wt16_loop(self):
        for f in Newforms(3, 6, names='a'):
            for p in prime_range(5, 50):
                for err in [0.1, 0.01, 0.000001]:
                    res = eigenvalue_numerical(f, p, err)
                    assert abs(res - f[p]) < err

    # this goes through all rational newforms of level 3
    def test_level3_loop(self):
        p = 17
        err = 0.1
        for k in range(2, 25, 2):
            for f in Newforms(3, k, names='a'):
                if f.base_ring().degree() == 1:
                    res = eigenvalue_numerical(f, p, err)
                    assert abs(res - f[p]) < err
