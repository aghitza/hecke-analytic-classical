import pytest
from sage.all_cmdline import CuspForms, Newforms, prime_range
from src.eigenvalue_sage import _initialise_rings, _eigenvalue_Tp_with_error


class TestEigenvalueWithError(object):

    def test_Delta12_2(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 12).gen(0)
        p = 2
        err = 0.1
        res, _, _ = _eigenvalue_Tp_with_error(f, z, p, err, 1)
        assert abs(res - f[p]) < err

    def test_Delta12_loop(self):
        CRING = _initialise_rings(55)
        z = CRING(0, 1)
        f = CuspForms(1, 12).gen(0)
        err = 0.1
        for p in prime_range(100):
            res, _, _ = _eigenvalue_Tp_with_error(f, z, p, err, 1)
            assert abs(res - f[p]) < err

    def test_Delta16_97(self):
        CRING = _initialise_rings(70)
        z = CRING(0, 1)
        f = CuspForms(1, 16).gen(0)
        p = 97
        err = 0.1
        res, _, _ = _eigenvalue_Tp_with_error(f, z, p, err, 1)
        assert abs(res - f[p]) < err

    def test_Delta18_23(self):
        CRING = _initialise_rings(60)
        z = CRING(0, 1.1)
        f = CuspForms(1, 18).gen(0)
        p = 23
        err = 0.1
        res, _, _ = _eigenvalue_Tp_with_error(f, z, p, err, 1)
        assert abs(res - f[p]) < err

    @pytest.mark.xfail
    def test_Delta18_loop(self):
        CRING = _initialise_rings(76)
        z = CRING(0, 1.1)
        f = CuspForms(1, 18).gen(0)
        err = 0.1
        for p in prime_range(100):
            res, _, _ = _eigenvalue_Tp_with_error(f, z, p, err, 1)
            assert abs(res - f[p]) < err

    def test_wt24_5(self):
        CRING = _initialise_rings()
        z = CRING(0, 1.1)
        f = Newforms(1, 24, names='a')[0]
        p = 5
        err = 0.1
        res, _, _ = _eigenvalue_Tp_with_error(f, z, p, err, 1)
        assert abs(res - CRING(f[p].complex_embedding(res.prec()))) < err

    def test_wt24_loop(self):
        CRING = _initialise_rings(100)
        z = CRING(0, 1.1)
        f = Newforms(1, 24, names='a')[0]
        for p in prime_range(50):
            for err in [0.1, 0.01, 0.000001]:
                res, _, _ = _eigenvalue_Tp_with_error(f, z, p, err, 1)
                assert abs(res - CRING(f[p].complex_embedding(res.prec()))) < err
