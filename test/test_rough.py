import pytest
from sage.all_cmdline import CuspForms
from src.eigenvalue_sage import _initialise_rings, _rough_estimate_F


class TestRoughEstimate(object):

    def test_estimate_Delta12_i(self):
        CRING = _initialise_rings()
        z = CRING(0, 1)
        f = CuspForms(1, 12).gen(0)
        res, eps = _rough_estimate_F(f, z, 1)
        assert abs(res) > 2*eps
        assert eps == 0.0001
        assert equal(0.001785, res)


EPS = 0.0005


def equal(left, right):
    return abs(left-right) < EPS


